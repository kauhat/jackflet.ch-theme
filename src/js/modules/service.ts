export default class {
  constructor () {
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('/worker.js').then((registration) => {
          // Registration was successful
        }, (err) => {
          // Registration failed
          console.log('Service worker registration failed: ', err)
        })
      })
    }
  }
}
