import * as WebFont from 'webfontloader'

export default class {
  constructor () {
    WebFont.load({
      google: {
        families: ['Lato']
      }
    })
  }
}
