declare global {
  interface Window {
    toggleGrid: any;
  }
}

export default class {
  constructor () {
    // Add develop class
    document.body.classList.add('dev--active')

    this.initGrid(document.body);
  }

  protected initGrid (elem: HTMLElement) {
    // Display baseline grid
    window.toggleGrid = function () {
      elem.classList.toggle('grid')
    }
  }

  // protected registerPaintWorklet () {
  //   if ('paintWorklet' in CSS) {
  //     CSS.paintWorklet.addModule('/worklets/checkerboard.js');
  //   }
  // }
}
