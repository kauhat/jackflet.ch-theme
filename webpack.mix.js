const mix = require('laravel-mix')

// Static files
mix.copyDirectory('src/static', 'static');

// Styles
mix.sass('src/sass/app.scss', 'static/css')

// Main scripts
mix.ts('src/js/app.ts', 'static/js')
  //.extract([
  //  'axios',
  //  'webfontloader',
  //  'vue'
  //])
