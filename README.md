# Theme

## Use Yarn/NPM
```shell
docker-compose run --rm js yarn
docker-compose run --rm js npm

...

docker-compose run --rm js yarn upgrade
docker-compose run --rm js npm update
```

## Build output CSS & JS
```shell
docker-compose run --rm js yarn run production
```

## Watch for changes
```shell
docker-compose run --rm js yarn run watch --watch-poll
# or abbr.
docker-compose run --rm js
```

## Clean up unused containers
```shell
docker-compose down
```
